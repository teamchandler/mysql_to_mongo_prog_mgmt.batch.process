﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace BatchProcess
{
    public partial class frmImpSalesTargetMonthly : Form
    {
        DataTable dtTargetSalesMonth;
        string serverURL = "http://localhost:8185/user/postSalesTargetMonthlyLava";
        public frmImpSalesTargetMonthly()
        {
            InitializeComponent();
            //serverURL = System.Configuration.ConfigurationSettings.AppSettings["SalesTargetMonthlyLave"].ToString();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtFile.Text = openFileDialog1.FileName;
            }
        }


        private void Upload()
        {
            System.IO.StreamReader sr = new System.IO.StreamReader(txtFile.Text);

            string strLine = "";
            int i = 0;

            string Mobile = "";
            string Email = "";
            string Sku = "";
            string TargetValue = "";
            string AchieveValue = "";
            string TargetVol = "";
            string AchieveVol = "";
            string Points = "";
            string StartDate = "";
            string EndDate = "";



            dtTargetSalesMonth = new DataTable();
            dtTargetSalesMonth.Rows.Clear();

            while (!sr.EndOfStream)
            {
                i += 1;
                strLine = sr.ReadLine();

                if (i > 1)
                {
                    string[] strSplit = strLine.Split(';');

                    if (strSplit.Length >= 9)
                    {

                        Mobile = strSplit[0];
                        Email = strSplit[1];
                        Sku = strSplit[2];
                        TargetValue = strSplit[3];
                        AchieveValue = strSplit[4];
                        TargetVol = strSplit[5];
                        AchieveVol = strSplit[6];
                        Points = strSplit[7];
                        StartDate = strSplit[8];
                        EndDate = strSplit[9];

                        addtoDataTable(Mobile, Email, Sku, TargetValue, AchieveValue, TargetVol, AchieveVol, Points, StartDate, EndDate);
                    }
                }
            }

        }

        private void addtoDataTable(string Mobile, string Email, string Sku, string TargetValue, string AchieveValue, string TargetVol, string AchieveVol, string Points, string StartDate, string EndDate)
        {

            DataRow dr = dtTargetSalesMonth.NewRow();

            if (dtTargetSalesMonth.Columns.Count == 0)
            {
                DataColumn dc0 = new DataColumn("Mobile");
                DataColumn dc1 = new DataColumn("Email");
                DataColumn dc2 = new DataColumn("Sku");
                DataColumn dc3 = new DataColumn("Target_Value");
                DataColumn dc4 = new DataColumn("Achieve_Value");
                DataColumn dc5 = new DataColumn("Target_Vol");
                DataColumn dc6 = new DataColumn("Achieve_Vol");
                DataColumn dc7 = new DataColumn("Points");
                DataColumn dc8 = new DataColumn("Start_Date");
                DataColumn dc9 = new DataColumn("End_Date");
                DataColumn dc10 = new DataColumn("Status");

                dtTargetSalesMonth.Columns.Add(dc0);
                dtTargetSalesMonth.Columns.Add(dc1);
                dtTargetSalesMonth.Columns.Add(dc2);
                dtTargetSalesMonth.Columns.Add(dc3);
                dtTargetSalesMonth.Columns.Add(dc4);
                dtTargetSalesMonth.Columns.Add(dc5);
                dtTargetSalesMonth.Columns.Add(dc6);
                dtTargetSalesMonth.Columns.Add(dc7);
                dtTargetSalesMonth.Columns.Add(dc8);
                dtTargetSalesMonth.Columns.Add(dc9);
                dtTargetSalesMonth.Columns.Add(dc10);

            }

            dr[0] = Mobile;
            dr[1] = Email;
            dr[2] = Sku;
            dr[3] = TargetValue;
            dr[4] = AchieveValue;
            dr[5] = TargetVol;
            dr[6] = AchieveVol;
            dr[7] = Points;
            dr[8] = StartDate;
            dr[9] = EndDate;
            dr[10] = "Not Processed Yet";
            dtTargetSalesMonth.Rows.Add(dr);

        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            if (txtFile.Text.Trim().Length == 0)
                return;
            Upload();

            dgView.Columns[0].DataPropertyName = "Mobile";
            dgView.Columns[1].DataPropertyName = "Email";
            dgView.Columns[2].DataPropertyName = "Sku";
            dgView.Columns[3].DataPropertyName = "Target_Value";
            dgView.Columns[4].DataPropertyName = "Achieve_Value";
            dgView.Columns[5].DataPropertyName = "Target_Vol";
            dgView.Columns[6].DataPropertyName = "Achieve_Vol";
            dgView.Columns[7].DataPropertyName = "Points";
            dgView.Columns[8].DataPropertyName = "Start_Date";
            dgView.Columns[9].DataPropertyName = "End_Date";
            dgView.Columns[10].DataPropertyName = "Status";

            dgView.DataSource = dtTargetSalesMonth;

            btnUpload.Enabled = false;
            btnProcess.Enabled = true;
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            string Mobile = "";
            string Email = "";
            string Sku = "";
            string TargetValue = "";
            string AchieveValue = "";
            string TargetVol = "";
            string AchieveVol = "";
            string Points = "";
            string StartDate = "";
            string EndDate = "";
            string status = "";
            string jsondata = "";
            servercomm sc = new servercomm();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            progressBar1.Visible = true;

            progressBar1.Minimum = 0;
            progressBar1.Value = 0;
            progressBar1.Maximum = dtTargetSalesMonth.Rows.Count;

            for (int i = 0; i < dtTargetSalesMonth.Rows.Count; i++)
            {
                try
                {
                    Mobile = dtTargetSalesMonth.Rows[i][0].ToString();
                    Email = dtTargetSalesMonth.Rows[i][1].ToString();
                    Sku = dtTargetSalesMonth.Rows[i][2].ToString();
                    TargetValue = dtTargetSalesMonth.Rows[i][3].ToString();
                    AchieveValue = dtTargetSalesMonth.Rows[i][4].ToString();
                    TargetVol = dtTargetSalesMonth.Rows[i][5].ToString();
                    AchieveVol = dtTargetSalesMonth.Rows[i][6].ToString();
                    Points = dtTargetSalesMonth.Rows[i][7].ToString();
                    StartDate = dtTargetSalesMonth.Rows[i][8].ToString();
                    EndDate = dtTargetSalesMonth.Rows[i][9].ToString();

                    jsondata = PrepareData(Mobile, Email, Sku, TargetValue, AchieveValue, TargetVol, AchieveVol, Points, StartDate, EndDate);

                    status = sc.postRequest(serverURL, jsondata);
                    status = ser.Deserialize<String>(status);

                    if (status.Trim().ToUpper().Contains("SUCCESSFULLY"))
                    {
                        dtTargetSalesMonth.Rows[i][10] = "Processed Successfully";
                    }
                    else if (status.Trim().ToUpper().Contains("DUPLICATE"))
                    {
                        dtTargetSalesMonth.Rows[i][10] = "Already Processed. Skipped";
                    }
                    else
                        dtTargetSalesMonth.Rows[i][10] = status;


                }
                catch (Exception ex)
                {
                    dtTargetSalesMonth.Rows[i][10] = status;
                }
                progressBar1.Value += 1;
                Application.DoEvents();
            }

            progressBar1.Visible = false;
            dtTargetSalesMonth.AcceptChanges();
            dgView.DataSource = dtTargetSalesMonth;
        }

        private string PrepareData(string Mobile, string Email, string Sku, string TargetValue, string AchieveValue, string TargetVol, string AchieveVol, string Points, string StartDate, string EndDate)
        {
            JavaScriptSerializer ser = new JavaScriptSerializer();
            Dictionary<string, string> thisDict = new Dictionary<string, string>();


            thisDict.Add("Mobile", Mobile);
            thisDict.Add("Email", Email);
            thisDict.Add("Sku", Sku);
            thisDict.Add("TargetValue", TargetValue);
            thisDict.Add("AchieveValue", AchieveValue);
            thisDict.Add("TargetVol", TargetVol);
            thisDict.Add("AchieveVol", AchieveVol);
            thisDict.Add("Points", Points);
            thisDict.Add("StartDate", StartDate);
            thisDict.Add("EndDate", EndDate);
           
            string JSON = ser.Serialize(thisDict);

            return JSON;

        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            btnUpload.Enabled = true;
            btnProcess.Enabled = false;
            dtTargetSalesMonth.Rows.Clear();
            dgView.DataSource = dtTargetSalesMonth;
        }

        private void frmMigrate_Shown(object sender, EventArgs e)
        {
            btnProcess.Enabled = false;

        }


    }
}
