﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace BatchProcess
{
    public partial class frmImpSalesTarget : Form
    {
        DataTable dtTargetSales;
        string serverURL = "http://localhost:8185/user/postSalesTarget";
        public frmImpSalesTarget()
        {
            InitializeComponent();
            //serverURL = System.Configuration.ConfigurationSettings.AppSettings["SalesTarget"].ToString();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtFile.Text = openFileDialog1.FileName;
            }
        }


        private void Upload()
        {
            System.IO.StreamReader sr = new System.IO.StreamReader(txtFile.Text);
            
            string strLine = "";
            int i = 0;

            string FirstName = "";
            string LastName = "";
            string Mobile = "";
            string Code = "";
            string Email = "";
            string Address = "";
            string City = "";
            string State = "";
            string ZipCode = "";
            string Company = "";
            string MotherMaidenName = "";
            string FirstSchool = "";
            string NativePlace = "";
            string Retailer = "";
            string DateOfBirth = "";
            string Target = "";
            string Achievement = "";
            string Points = "";
            string StartDate = "";
            string EndDate = "";

            dtTargetSales = new DataTable();
            dtTargetSales.Rows.Clear();

            while (!sr.EndOfStream)
            {
                i += 1;
                strLine = sr.ReadLine();

                if (i > 1)
                {
                    string[] strSplit = strLine.Split(';');

                    if (strSplit.Length >= 19)
                    {

                        FirstName = strSplit[0];
                        LastName = strSplit[1];
                        Mobile = strSplit[2];
                        Code = strSplit[3];
                        Email = strSplit[4];
                        Address = strSplit[5];
                        City = strSplit[6];
                        State = strSplit[7];
                        ZipCode = strSplit[8];
                        Company = strSplit[9];
                        MotherMaidenName = strSplit[10];
                        FirstSchool = strSplit[11];
                        NativePlace = strSplit[12];
                        Retailer = strSplit[13];
                        DateOfBirth = strSplit[14];
                        Target = strSplit[15];
                        Achievement = strSplit[16];
                        Points = strSplit[17];
                        StartDate = strSplit[18];
                        EndDate = strSplit[19];

                        addtoDataTable(FirstName, LastName, Mobile, Code, Email, Address, City, State, ZipCode, Company,MotherMaidenName, FirstSchool, NativePlace, Retailer, DateOfBirth, Target, Achievement, Points, StartDate, EndDate);
                    }
                }
            }

        }

        private void addtoDataTable(string FirstName, string LastName, string Mobile, string Code, string Email, string Address, string City, string State, string ZipCode, string Company, string MotherMaidenName, string FirstSchool, string NativePlace, string Retailer, string DateOfBirth, string Target, string Achievement, string Points, string StartDate, string EndDate)
        {

            DataRow dr = dtTargetSales.NewRow();

            if (dtTargetSales.Columns.Count == 0)
            {
                DataColumn dc0 = new DataColumn("First_Name");
                DataColumn dc1 = new DataColumn("Last_Name");
                DataColumn dc2 = new DataColumn("Mobile");
                DataColumn dc3 = new DataColumn("Code");
                DataColumn dc4 = new DataColumn("Email");
                DataColumn dc5 = new DataColumn("Address");
                DataColumn dc6 = new DataColumn("City");
                DataColumn dc7 = new DataColumn("State");
                DataColumn dc8 = new DataColumn("Zip_Code");
                DataColumn dc9 = new DataColumn("Company");
                DataColumn dc10 = new DataColumn("Mother_Maiden_Name");
                DataColumn dc11 = new DataColumn("First_School");
                DataColumn dc12 = new DataColumn("Native_Place");
                DataColumn dc13 = new DataColumn("Retailer");
                DataColumn dc14 = new DataColumn("Date_Of_Birth");
                DataColumn dc15 = new DataColumn("Target");
                DataColumn dc16 = new DataColumn("Achievement");
                DataColumn dc17 = new DataColumn("Points");
                DataColumn dc18 = new DataColumn("Start_Date");
                DataColumn dc19 = new DataColumn("End_Date");
                DataColumn dc20 = new DataColumn("Status");


                dtTargetSales.Columns.Add(dc0);
                dtTargetSales.Columns.Add(dc1);
                dtTargetSales.Columns.Add(dc2);
                dtTargetSales.Columns.Add(dc3);
                dtTargetSales.Columns.Add(dc4);
                dtTargetSales.Columns.Add(dc5);
                dtTargetSales.Columns.Add(dc6);
                dtTargetSales.Columns.Add(dc7);
                dtTargetSales.Columns.Add(dc8);
                dtTargetSales.Columns.Add(dc9);
                dtTargetSales.Columns.Add(dc10);
                dtTargetSales.Columns.Add(dc11);
                dtTargetSales.Columns.Add(dc12);
                dtTargetSales.Columns.Add(dc13);
                dtTargetSales.Columns.Add(dc14);
                dtTargetSales.Columns.Add(dc15);
                dtTargetSales.Columns.Add(dc16);
                dtTargetSales.Columns.Add(dc17);
                dtTargetSales.Columns.Add(dc18);
                dtTargetSales.Columns.Add(dc19);
                dtTargetSales.Columns.Add(dc20);
            }

            dr[0] = FirstName;
            dr[1] = LastName;
            dr[2] = Mobile;
            dr[3] = Code;
            dr[4] = Email;
            dr[5] = Address;
            dr[6] = City;
            dr[7] = State;
            dr[8] = ZipCode;
            dr[9] = Company;
            dr[10] = MotherMaidenName;
            dr[11] = FirstSchool;
            dr[12] = NativePlace;
            dr[13] = Retailer;
            dr[14] = DateOfBirth;
            dr[15] = Target;
            dr[16] = Achievement;
            dr[17] = Points;
            dr[18] = StartDate;
            dr[19] = EndDate;
            dr[20] = "Not Processed Yet";
            dtTargetSales.Rows.Add(dr);

        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            if (txtFile.Text.Trim().Length == 0)
                return;
            Upload();

            dgView.Columns[0].DataPropertyName = "First_Name";
            dgView.Columns[1].DataPropertyName = "Last_Name";
            dgView.Columns[2].DataPropertyName = "Mobile";
            dgView.Columns[3].DataPropertyName = "Code";
            dgView.Columns[4].DataPropertyName = "Email";
            dgView.Columns[5].DataPropertyName = "Address";
            dgView.Columns[6].DataPropertyName = "City";
            dgView.Columns[7].DataPropertyName = "State";
            dgView.Columns[8].DataPropertyName = "Zip_Code";
            dgView.Columns[9].DataPropertyName = "Company";
            dgView.Columns[10].DataPropertyName = "Mother_Maiden_Name";
            dgView.Columns[11].DataPropertyName = "First_School";
            dgView.Columns[12].DataPropertyName = "Native_Place";
            dgView.Columns[13].DataPropertyName = "Retailer";
            dgView.Columns[14].DataPropertyName = "Date_Of_Birth";
            dgView.Columns[15].DataPropertyName = "Target";
            dgView.Columns[16].DataPropertyName = "Achievement";
            dgView.Columns[17].DataPropertyName = "Points";
            dgView.Columns[18].DataPropertyName = "Start_Date";
            dgView.Columns[19].DataPropertyName = "End_Date";
            dgView.Columns[20].DataPropertyName = "Status";

            dgView.DataSource = dtTargetSales;

            btnUpload.Enabled = false;
            btnProcess.Enabled = true;
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            string FirstName = "";
            string LastName = "";
            string Mobile = "";
            string Code = "";
            string Email = "";
            string Address = "";
            string City = "";
            string State = "";
            string ZipCode = "";
            string Company = "";
            string MotherMaidenName = "";
            string FirstSchool = "";
            string NativePlace = "";
            string Retailer = "";
            string DateOfBirth = "";
            string Target = "";
            string Achievement = "";
            string Points = "";
            string StartDate = "";
            string EndDate = "";
            string status = "";
            string jsondata = "";
            servercomm sc = new servercomm();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            progressBar1.Visible = true;

            progressBar1.Minimum = 0;
            progressBar1.Value = 0;
            progressBar1.Maximum = dtTargetSales.Rows.Count;

            for (int i = 0; i < dtTargetSales.Rows.Count; i++)
            {
                try
                {
                    FirstName = dtTargetSales.Rows[i][0].ToString();
                    LastName = dtTargetSales.Rows[i][1].ToString();
                    Mobile = dtTargetSales.Rows[i][2].ToString();
                    Code = dtTargetSales.Rows[i][3].ToString();
                    Email = dtTargetSales.Rows[i][4].ToString();
                    Address = dtTargetSales.Rows[i][5].ToString();
                    City = dtTargetSales.Rows[i][6].ToString();
                    State= dtTargetSales.Rows[i][7].ToString();
                    ZipCode	= dtTargetSales.Rows[i][8].ToString();
                    Company = dtTargetSales.Rows[i][9].ToString();
                    MotherMaidenName	= dtTargetSales.Rows[i][10].ToString();
                    FirstSchool	= dtTargetSales.Rows[i][11].ToString();
                    NativePlace	= dtTargetSales.Rows[i][12].ToString();
                    Retailer	= dtTargetSales.Rows[i][13].ToString();
                    DateOfBirth	= dtTargetSales.Rows[i][14].ToString();
                    Target	= dtTargetSales.Rows[i][15].ToString();
                    Achievement	= dtTargetSales.Rows[i][16].ToString();
                    Points	= dtTargetSales.Rows[i][17].ToString();
                    StartDate = dtTargetSales.Rows[i][18].ToString();
                    EndDate = dtTargetSales.Rows[i][19].ToString();

                    jsondata = PrepareData(FirstName, LastName, Mobile, Code, Email, Address, City, State, ZipCode,Company, MotherMaidenName, FirstSchool, NativePlace, Retailer, DateOfBirth, Target, Achievement, Points, StartDate, EndDate);

                    status = sc.postRequest(serverURL, jsondata);
                    status = ser.Deserialize<String>(status);

                    if (status.Trim().ToUpper().Contains("SUCCESSFULLY"))
                    {
                        dtTargetSales.Rows[i][20] = "Processed Successfully";
                    }
                    else if (status.Trim().ToUpper().Contains("DUPLICATE"))
                    {
                        dtTargetSales.Rows[i][20] = "Already Processed. Skipped";
                    }
                    else
                        dtTargetSales.Rows[i][20] = status;


                }
                catch (Exception ex)
                {
                    dtTargetSales.Rows[i][20] = status;
                }
                progressBar1.Value += 1;
                Application.DoEvents();
            }

            progressBar1.Visible = false;
            dtTargetSales.AcceptChanges();
            dgView.DataSource = dtTargetSales;
        }

        private string PrepareData(string FirstName, string LastName, string Mobile, string Code, string Email, string Address, string City, string State, string ZipCode, string Company, string MotherMaidenName, string FirstSchool, string NativePlace, string Retailer, string DateOfBirth, string Target, string Achievement, string Points, string StartDate, string EndDate)
        {
            JavaScriptSerializer ser = new JavaScriptSerializer();
            Dictionary<string, string> thisDict = new Dictionary<string, string>();
            thisDict.Add("first_name", FirstName);
            thisDict.Add("last_name", LastName);
            thisDict.Add("mobile_no", Mobile);
            thisDict.Add("code", Code);
            thisDict.Add("email_id", Email);
            thisDict.Add("street", Address);
            thisDict.Add("city", City);
            thisDict.Add("state", State);
            thisDict.Add("zipcode", ZipCode);
            thisDict.Add("company", Company);
            thisDict.Add("Mother_Maiden_Name", MotherMaidenName);
            thisDict.Add("First_School", FirstSchool);
            thisDict.Add("Native_Place", NativePlace);
            thisDict.Add("Retailer", Retailer);
            thisDict.Add("Date_Of_Birth", DateOfBirth);
            thisDict.Add("Target", Target);
            thisDict.Add("Achievement", Achievement);
            thisDict.Add("Points", Points);
            thisDict.Add("Start_Date", StartDate);
            thisDict.Add("End_Date", EndDate);

            string JSON = ser.Serialize(thisDict);

            return JSON;

        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            btnUpload.Enabled = true;
            btnProcess.Enabled = false;
            dtTargetSales.Rows.Clear();
            dgView.DataSource = dtTargetSales;
        }

        private void frmMigrate_Shown(object sender, EventArgs e)
        {
            btnProcess.Enabled = false;

        }


    }
}
