﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace BatchProcess
{
    public partial class frmImpDataForLava : Form
    {
        DataTable dtLavaData;
        //string serverURL = "http://localhost:8185/user/postLavaData";
        //string GetCompURL = "http://localhost:8185/user/admin/postPopulateCompany";

        string serverURL = "";
        string GetCompURL = "";
        string LavaUrl = "";

        public frmImpDataForLava()
        {
            InitializeComponent();
            serverURL = System.Configuration.ConfigurationSettings.AppSettings["ServerURL"].ToString();
            LavaUrl = serverURL + "/user/postLavaData";
            GetCompURL = serverURL + "/user/admin/postPopulateCompany";

            PopulateCompany();
          
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtFile.Text = openFileDialog1.FileName;
            }
        }


        private void PopulateCompany()
        {

            string CompData = "";
            List<COMPANY> CompList = new List<COMPANY>();
            servercomm scc = new servercomm();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            CompData = scc.postRequest(GetCompURL, "");
            CompList = ser.Deserialize<List<COMPANY>>(CompData);

            if (CompList != null && CompList.Count() > 0)
            {
                List<ComboboxItem> liCombo = new List<ComboboxItem>();
                for (int i = 1; i < CompList.Count(); i++)
                {
                    ComboboxItem item = new ComboboxItem();
                    item.Text = CompList[i].name;
                    item.Value = CompList[i].id;
                    liCombo.Add(item);
                }
                this.ddlCompany.DataSource = liCombo;
            }


        }

        private class ComboboxItem
        {
            public string Text { get; set; }
            public object Value { get; set; }

            public override string ToString()
            {
                return Text;
            }
        }
        private string convertToString(object o)
        {
            try
            {
                if ((o != null) && (o.ToString() != ""))
                {
                    return o.ToString().Trim();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {

                return "";
            }
        }

        private class COMPANY
        {
            public long id { get; set; }
            public string name { get; set; }
            public string displayname { get; set; }
            public string contactname { get; set; }
            public string phonenumber { get; set; }
            public string address { get; set; }
            public string emailid { get; set; }
            public string logo { get; set; }
            public decimal point_range_max { get; set; }
            public decimal point_range_min { get; set; }
            public string root_url_1 { get; set; }
            public string root_url_2 { get; set; }
            public decimal shipping_charge { get; set; }
            public decimal min_order_for_free_shipping { get; set; }


            public int sales_tracking { get; set; }
            public string track_cycle { get; set; }
            public int validity { get; set; }

        }
        private void Upload()
        {
            System.IO.StreamReader sr = new System.IO.StreamReader(txtFile.Text);

            string strLine = "";
            int i = 0;

            string FirstName = "";
            string LastName = "";
            string DateOfBirth = "";
            string DateOfJoiningLAVA = "";
            string Qualifications = "";
            string DSEContactNo = "";
            string DSEEMailId = "";
            string DSEPostalAddress = "";
            string DecValueAchivement = "";
            string BranchManager = "";
            string BranchName = "";
            string Status = "";
            string DSEPoints = "";
            string CompanyId = "";
            string SelesCycleId = "";


            dtLavaData = new DataTable();
            dtLavaData.Rows.Clear();

            while (!sr.EndOfStream)
            {
                i += 1;
                strLine = sr.ReadLine();

                if (i > 1)
                {
                    string[] strSplit = strLine.Split(';');

                    if (strSplit.Length >= 13)
                    {

                        FirstName = convertToString(strSplit[0]);
                        LastName = convertToString(strSplit[1]);
                        DateOfBirth = convertToString(strSplit[2]);
                        DateOfJoiningLAVA = convertToString(strSplit[3]);
                        Qualifications = convertToString(strSplit[4]);
                        DSEContactNo = convertToString(strSplit[5]);
                        DSEEMailId = convertToString(strSplit[6]);
                        DSEPostalAddress = convertToString(strSplit[7]);
                        DecValueAchivement = convertToString(strSplit[8]);
                        BranchManager = convertToString(strSplit[9]);
                        BranchName = convertToString(strSplit[10]);
                        Status = convertToString(strSplit[11]);
                        DSEPoints = convertToString(strSplit[12]);


                        addtoDataTable(FirstName, LastName, DateOfBirth, DateOfJoiningLAVA, Qualifications, DSEContactNo, DSEEMailId, DSEPostalAddress, DecValueAchivement, BranchManager, BranchName, Status, DSEPoints);
                    }
                }
            }

        }

        private void addtoDataTable(string FirstName, string LastName, string DateOfBirth, string DateOfJoiningLAVA, string Qualifications, string DSEContactNo, string DSEEMailId, string DSEPostalAddress, string DecValueAchivement, string BranchManager, string BranchName, string Status, string DSEPoints)
        {


            DataRow dr = dtLavaData.NewRow();

            if (dtLavaData.Columns.Count == 0)
            {
                DataColumn dc0 = new DataColumn("First_Name");
                DataColumn dc1 = new DataColumn("Last_Name");
                DataColumn dc2 = new DataColumn("DateOfBirth");
                DataColumn dc3 = new DataColumn("DateOfJoiningLAVA");
                DataColumn dc4 = new DataColumn("Qualifications");
                DataColumn dc5 = new DataColumn("DSEContactNo");
                DataColumn dc6 = new DataColumn("DSEEMailId");
                DataColumn dc7 = new DataColumn("DSEPostalAddress");
                DataColumn dc8 = new DataColumn("DecValueAchivement");
                DataColumn dc9 = new DataColumn("BranchManager");
                DataColumn dc10 = new DataColumn("BranchName");
                DataColumn dc11 = new DataColumn("Status");
                DataColumn dc12 = new DataColumn("DSEPoints");
                DataColumn dc13 = new DataColumn("UploadStatus");



                dtLavaData.Columns.Add(dc0);
                dtLavaData.Columns.Add(dc1);
                dtLavaData.Columns.Add(dc2);
                dtLavaData.Columns.Add(dc3);
                dtLavaData.Columns.Add(dc4);
                dtLavaData.Columns.Add(dc5);
                dtLavaData.Columns.Add(dc6);
                dtLavaData.Columns.Add(dc7);
                dtLavaData.Columns.Add(dc8);
                dtLavaData.Columns.Add(dc9);
                dtLavaData.Columns.Add(dc10);
                dtLavaData.Columns.Add(dc11);
                dtLavaData.Columns.Add(dc12);
                dtLavaData.Columns.Add(dc13);


            }

            dr[0] = FirstName;
            dr[1] = LastName;
            dr[2] = DateOfBirth;
            dr[3] = DateOfJoiningLAVA;
            dr[4] = Qualifications;
            dr[5] = DSEContactNo;
            dr[6] = DSEEMailId;
            dr[7] = DSEPostalAddress;
            dr[8] = DecValueAchivement;
            dr[9] = BranchManager;
            dr[10] = BranchName;
            dr[11] = Status;
            dr[12] = DSEPoints;

            dr[13] = "Not Processed Yet";
            dtLavaData.Rows.Add(dr);

        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            if (txtFile.Text.Trim().Length == 0)
                return;
            Upload();

            dgView.Columns[0].DataPropertyName = "First_Name";
            dgView.Columns[1].DataPropertyName = "Last_Name";
            dgView.Columns[2].DataPropertyName = "DateOfBirth";
            dgView.Columns[3].DataPropertyName = "DateOfJoiningLAVA";
            dgView.Columns[4].DataPropertyName = "Qualifications";
            dgView.Columns[5].DataPropertyName = "DSEContactNo";
            dgView.Columns[6].DataPropertyName = "DSEEMailId";
            dgView.Columns[7].DataPropertyName = "DSEPostalAddress";
            dgView.Columns[8].DataPropertyName = "DecValueAchivement";
            dgView.Columns[9].DataPropertyName = "BranchManager";
            dgView.Columns[10].DataPropertyName = "BranchName";
            dgView.Columns[11].DataPropertyName = "Status";
            dgView.Columns[12].DataPropertyName = "DSEPoints";
            dgView.Columns[13].DataPropertyName = "UploadStatus";

            dgView.DataSource = dtLavaData;

            btnUpload.Enabled = false;
            btnProcess.Enabled = true;
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            string FirstName = "";
            string LastName = "";
            string DateOfBirth = "";
            string DateOfJoiningLAVA = "";
            string Qualifications = "";
            string DSEContactNo = "";
            string DSEEMailId = "";
            string DSEPostalAddress = "";
            string DecValueAchivement = "";
            string BranchManager = "";
            string BranchName = "";
            string Status = "";
            string DSEPoints = "";
            string UploadStatus = "";


            string jsondata = "";
            servercomm sc = new servercomm();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            progressBar1.Visible = true;

            progressBar1.Minimum = 0;
            progressBar1.Value = 0;
            progressBar1.Maximum = dtLavaData.Rows.Count;

            for (int i = 0; i < dtLavaData.Rows.Count; i++)
            {
                try
                {

                    FirstName = dtLavaData.Rows[i][0].ToString();
                    LastName = dtLavaData.Rows[i][1].ToString();
                    DateOfBirth = dtLavaData.Rows[i][2].ToString();
                    DateOfJoiningLAVA = dtLavaData.Rows[i][3].ToString();
                    Qualifications = dtLavaData.Rows[i][4].ToString();
                    DSEContactNo = dtLavaData.Rows[i][5].ToString();
                    DSEEMailId = dtLavaData.Rows[i][6].ToString();
                    DSEPostalAddress = dtLavaData.Rows[i][7].ToString();
                    DecValueAchivement = dtLavaData.Rows[i][8].ToString();
                    BranchManager = dtLavaData.Rows[i][9].ToString();
                    BranchName = dtLavaData.Rows[i][10].ToString();
                    Status = dtLavaData.Rows[i][11].ToString();
                    DSEPoints = dtLavaData.Rows[i][12].ToString();

                    jsondata = PrepareData(FirstName, LastName, DateOfBirth, DateOfJoiningLAVA, Qualifications, DSEContactNo, DSEEMailId, DSEPostalAddress, DecValueAchivement, BranchManager, BranchName, Status, DSEPoints);

                    UploadStatus = sc.postRequest(LavaUrl, jsondata);
                    UploadStatus = ser.Deserialize<String>(UploadStatus);

                    if (UploadStatus.Trim().ToUpper().Contains("SUCCESSFULLY"))
                    {
                        dtLavaData.Rows[i][13] = "Processed Successfully";
                    }
                    else if (UploadStatus.Trim().ToUpper().Contains("DUPLICATE"))
                    {
                        dtLavaData.Rows[i][13] = "Already Processed. Skipped";
                    }
                    else
                        dtLavaData.Rows[i][13] = UploadStatus;


                }
                catch (Exception ex)
                {
                    dtLavaData.Rows[i][13] = UploadStatus;
                }
                progressBar1.Value += 1;
                Application.DoEvents();
            }

            progressBar1.Visible = false;
            dtLavaData.AcceptChanges();
            dgView.DataSource = dtLavaData;
        }

        private string PrepareData(string FirstName, string LastName, string DateOfBirth, string DateOfJoiningLAVA, string Qualifications, string DSEContactNo, string DSEEMailId, string DSEPostalAddress, string DecValueAchivement, string BranchManager, string BranchName, string Status, string DSEPoints)
        {
            JavaScriptSerializer ser = new JavaScriptSerializer();
            Dictionary<string, string> thisDict = new Dictionary<string, string>();

            string CompanyId = "";
            string SelesCycleId = "";
            CompanyId = convertToString(((BatchProcess.frmImpDataForLava.ComboboxItem)(ddlCompany.SelectedItem)).Value);
            SelesCycleId = convertToString(txtSalesCycleId.Text);

            thisDict.Add("FirstName", FirstName);
            thisDict.Add("LastName", LastName);
            thisDict.Add("DateOfBirth", DateOfBirth);
            thisDict.Add("DateOfJoiningLAVA", DateOfJoiningLAVA);
            thisDict.Add("Qualifications", Qualifications);
            thisDict.Add("DSEContactNo", DSEContactNo);
            thisDict.Add("DSEEMailId", DSEEMailId);
            thisDict.Add("DSEPostalAddress", DSEPostalAddress);
            thisDict.Add("DecValueAchivement", DecValueAchivement);
            thisDict.Add("BranchManager", BranchManager);
            thisDict.Add("BranchName", BranchName);
            thisDict.Add("Status", Status);
            thisDict.Add("DSEPoints", DSEPoints);
            thisDict.Add("CompanyId", CompanyId);
            thisDict.Add("SelesCycleId", SelesCycleId);


            string JSON = ser.Serialize(thisDict);

            return JSON;

        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            btnUpload.Enabled = true;
            btnProcess.Enabled = false;
            dtLavaData.Rows.Clear();
            dgView.DataSource = dtLavaData;
        }

        private void frmMigrate_Shown(object sender, EventArgs e)
        {
            btnProcess.Enabled = false;

        }




    }
}
