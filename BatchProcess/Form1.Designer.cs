﻿namespace BatchProcess
{
    partial class frmBatchConsole
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.batchProcessToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uploadPointsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importCustomersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importSalesTargetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importSalesTargetMonthToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importLavaDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();

            this.importXoloDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();

            this.importSchneiderDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importGrassresellerDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();

            this.importSalesTargetMonthlyAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.batchProcessToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(884, 25);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // batchProcessToolStripMenuItem
            // 
            this.batchProcessToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importGrassresellerDataToolStripMenuItem,
            //this.uploadPointsToolStripMenuItem,
            //this.importCustomersToolStripMenuItem,this.importSalesTargetToolStripMenuItem,this.importSalesTargetMonthlyAllToolStripMenuItem,this.importSalesTargetMonthToolStripMenuItem,this.importLavaDataToolStripMenuItem,this.importXoloDataToolStripMenuItem,this.importSchneiderDataToolStripMenuItem
            });
            this.batchProcessToolStripMenuItem.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.batchProcessToolStripMenuItem.Name = "batchProcessToolStripMenuItem";
            this.batchProcessToolStripMenuItem.Size = new System.Drawing.Size(117, 21);
            this.batchProcessToolStripMenuItem.Text = "Batch Process";
            // 
            // uploadPointsToolStripMenuItem
            // 
            this.uploadPointsToolStripMenuItem.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uploadPointsToolStripMenuItem.Name = "uploadPointsToolStripMenuItem";
            this.uploadPointsToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.uploadPointsToolStripMenuItem.Text = "Upload Points";
            this.uploadPointsToolStripMenuItem.Click += new System.EventHandler(this.uploadPointsToolStripMenuItem_Click);
            
            // 
            // importCustomersToolStripMenuItem
            // 
            this.importCustomersToolStripMenuItem.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.importCustomersToolStripMenuItem.Name = "importCustomersToolStripMenuItem";
            this.importCustomersToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.importCustomersToolStripMenuItem.Text = "Import Customers";
            this.importCustomersToolStripMenuItem.Click += new System.EventHandler(this.importCustomersToolStripMenuItem_Click);

            // 
            // importSalesTargetToolStripMenuItem
            // 
            this.importSalesTargetToolStripMenuItem.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.importSalesTargetToolStripMenuItem.Name = "importSalesTargetToolStripMenuItem";
            this.importSalesTargetToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.importSalesTargetToolStripMenuItem.Text = "Import Sales Target";
            this.importSalesTargetToolStripMenuItem.Click += new System.EventHandler(this.importSalesTargetToolStripMenuItem_Click);

            // 
            // importSalesTargetMonthlyAllToolStripMenuItem
            // 
            this.importSalesTargetMonthlyAllToolStripMenuItem.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.importSalesTargetMonthlyAllToolStripMenuItem.Name = "importSalesTargetMonthToolStripMenuItem";
            this.importSalesTargetMonthlyAllToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.importSalesTargetMonthlyAllToolStripMenuItem.Text = "Import Sales Target Monthly";
            this.importSalesTargetMonthlyAllToolStripMenuItem.Click += new System.EventHandler(this.importSalesTargetMonthlyAllToolStripMenuItem_Click);

            // 
            // importSalesTargetMonthToolStripMenuItem
            // 
            this.importSalesTargetMonthToolStripMenuItem.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.importSalesTargetMonthToolStripMenuItem.Name = "importSalesTargetMonthToolStripMenuItem";
            this.importSalesTargetMonthToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.importSalesTargetMonthToolStripMenuItem.Text = "Import Sales Target Monthly For Lava";
            this.importSalesTargetMonthToolStripMenuItem.Click += new System.EventHandler(this.importSalesTargetMonthToolStripMenuItem_Click);



            // 
            // importLavaDataToolStripMenuItem
            // 
            this.importLavaDataToolStripMenuItem.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.importLavaDataToolStripMenuItem.Name = "importLavaDataToolStripMenuItem";
            this.importLavaDataToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.importLavaDataToolStripMenuItem.Text = "Import Data For Lava";
            this.importLavaDataToolStripMenuItem.Click += new System.EventHandler(this.importLavaDataToolStripMenuItem_Click);


            // 
            // importXoloDataToolStripMenuItem
            // 
            this.importXoloDataToolStripMenuItem.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.importXoloDataToolStripMenuItem.Name = "importXoloDataToolStripMenuItem";
            this.importXoloDataToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.importXoloDataToolStripMenuItem.Text = "Import Data For Xolo";
            this.importXoloDataToolStripMenuItem.Click += new System.EventHandler(this.importXoloDataToolStripMenuItem_Click);


            // 
            // importSchneiderDataToolStripMenuItem
            // 
            this.importSchneiderDataToolStripMenuItem.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.importSchneiderDataToolStripMenuItem.Name = "importSchneiderDataToolStripMenuItem";
            this.importSchneiderDataToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.importSchneiderDataToolStripMenuItem.Text = "Import Data For Schneider";
            this.importSchneiderDataToolStripMenuItem.Click += new System.EventHandler(this.importSchneiderDataToolStripMenuItem_Click);

            
            // 
            // importGrassresellerDataToolStripMenuItem
            // 
            this.importGrassresellerDataToolStripMenuItem.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.importGrassresellerDataToolStripMenuItem.Name = "importGrassresellerDataToolStripMenuItem";
            this.importGrassresellerDataToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.importGrassresellerDataToolStripMenuItem.Text = "Import Data For Grassreseller";
            this.importGrassresellerDataToolStripMenuItem.Click += new System.EventHandler(this.importGrassresellerDataToolStripMenuItem_Click);



            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 25);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(884, 383);
            this.panel1.TabIndex = 1;
            // 
            // frmBatchConsole
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(884, 408);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmBatchConsole";
            this.Text = "Batch Console";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Shown += new System.EventHandler(this.frmBatchConsole_Shown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem batchProcessToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uploadPointsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importCustomersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importSalesTargetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importSalesTargetMonthToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importLavaDataToolStripMenuItem;

        private System.Windows.Forms.ToolStripMenuItem importXoloDataToolStripMenuItem;

        private System.Windows.Forms.ToolStripMenuItem importSchneiderDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importGrassresellerDataToolStripMenuItem;

        private System.Windows.Forms.ToolStripMenuItem importSalesTargetMonthlyAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
    }
}

