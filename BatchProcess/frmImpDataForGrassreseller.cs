﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace BatchProcess
{
    public partial class frmImpDataForGrassreseller : Form
    {
        DataTable dtSchneiderData;
        //string serverURL = "http://localhost:8185/user/postLavaData";
        //string GetCompURL = "http://localhost:8185/user/admin/postPopulateCompany";

        string serverURL = "";
        string GetCompURL = "";
        string UserImportUrl = "";

        public frmImpDataForGrassreseller()
        {
            InitializeComponent();
            serverURL = System.Configuration.ConfigurationSettings.AppSettings["ServerURL"].ToString();
            UserImportUrl = serverURL + "/user/postGrassresellerData";
            GetCompURL = serverURL + "/user/admin/postCompanyList/";

            PopulateCompany();

        }

        private void btnSelect_Click(object sender, EventArgs e)
        {

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtFile.Text = openFileDialog1.FileName;
            }
        }


        private void PopulateCompany()
        {

            string CompData = "";
            List<COMPANY> CompList = new List<COMPANY>();
            servercomm scc = new servercomm();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            CompData = scc.postRequest(GetCompURL, "");
            CompList = ser.Deserialize<List<COMPANY>>(CompData);

            if (CompList != null && CompList.Count() > 0)
            {
                List<ComboboxItem> liCombo = new List<ComboboxItem>();

                ComboboxItem item1 = new ComboboxItem();
                item1.Text = "Select Company";
                item1.Value = "0";
                liCombo.Add(item1);

                for (int i = 0; i < CompList.Count(); i++)
                {
                    ComboboxItem item = new ComboboxItem();
                    item.Text = CompList[i].name;
                    item.Value = CompList[i].id;
                    liCombo.Add(item);
                }
                this.ddlComp.DataSource = liCombo;
            }


        }

        private class ComboboxItem
        {
            public string Text { get; set; }
            public object Value { get; set; }

            public override string ToString()
            {
                return Text;
            }
        }
        private string convertToString(object o)
        {
            try
            {
                if ((o != null) && (o.ToString() != ""))
                {
                    return o.ToString().Trim();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {

                return "";
            }
        }

        private class COMPANY
        {
            public double id { get; set; }
            public string name { get; set; }
            public string displayname { get; set; }
            public string contactname { get; set; }
            public string phonenumber { get; set; }
            public string address { get; set; }
            public string emailid { get; set; }
            public string logo { get; set; }
            public string company_type { get; set; }
            public double point_range_min { get; set; }
            public double point_range_max { get; set; }
            public string root_url_1 { get; set; }
            public string root_url_2 { get; set; }
            public double shipping_charge { get; set; }
            public double min_order_for_free_shipping { get; set; }
            public double sales_tracking { get; set; }
            public string track_cycle { get; set; }
            public double validity { get; set; }
            public double inr_point_ratio { get; set; }

        }
        private void Upload()
        {
            System.IO.StreamReader sr = new System.IO.StreamReader(txtFile.Text);

            string strLine = "";
            int i = 0;

            string First_Name = "";
            string Last_Name = "";
            string MobileNo = "";
            string Email_ID = "";
            string Additional_Email = "";
            string Company_Name = "";
            string Designation = "";
            string Partner_ID = "";
            string Zone = "";
            string Address = "";
            string City = "";
            string State = "";
           // string CompanyId = "";
           



            dtSchneiderData = new DataTable();
            dtSchneiderData.Rows.Clear();

            while (!sr.EndOfStream)
            {
                i += 1;
                strLine = sr.ReadLine();

                if (i > 1)
                {
                    string[] strSplit = strLine.Split(';');

                    if (strSplit.Length >= 11)
                    {

                        First_Name = convertToString(strSplit[0]);
                        Last_Name = convertToString(strSplit[1]);
                        MobileNo = convertToString(strSplit[2]);
                        Email_ID = convertToString(strSplit[3]);
                        Additional_Email = convertToString(strSplit[4]);
                        Company_Name = convertToString(strSplit[5]);
                        Designation = convertToString(strSplit[6]);
                        Partner_ID = convertToString(strSplit[7]);
                        Zone = convertToString(strSplit[8]);
                        Address = convertToString(strSplit[9]);
                        City = convertToString(strSplit[10]);
                        State = convertToString(strSplit[11]);
                      
                        addtoDataTable(First_Name, Last_Name, MobileNo, Email_ID,Additional_Email, Company_Name, Designation, Partner_ID, Zone, Address, City, State);

                    }
                }
            }

        }

        private void addtoDataTable(string First_Name, string Last_Name, string MobileNo, string Email_ID, string Additional_Email, string Company_Name, string Designation, string Partner_ID, string Zone, string Address, string City, string State)
        {


            DataRow dr = dtSchneiderData.NewRow();

            if (dtSchneiderData.Columns.Count == 0)
            {
                DataColumn dc0 = new DataColumn("First_Name");
                DataColumn dc1 = new DataColumn("Last_Name");
                DataColumn dc2 = new DataColumn("MobileNo");
                DataColumn dc3 = new DataColumn("Email_ID");
                DataColumn dc4 = new DataColumn("Additional_Email");
                DataColumn dc5 = new DataColumn("Company_Name");
                DataColumn dc6 = new DataColumn("Designation");
                DataColumn dc7 = new DataColumn("Partner_ID");
                DataColumn dc8 = new DataColumn("Zone");
                DataColumn dc9 = new DataColumn("Address");
                DataColumn dc10 = new DataColumn("City");
                DataColumn dc11 = new DataColumn("State");             
                DataColumn dc12 = new DataColumn("UploadStatus");


                dtSchneiderData.Columns.Add(dc0);
                dtSchneiderData.Columns.Add(dc1);
                dtSchneiderData.Columns.Add(dc2);
                dtSchneiderData.Columns.Add(dc3);
                dtSchneiderData.Columns.Add(dc4);
                dtSchneiderData.Columns.Add(dc5);
                dtSchneiderData.Columns.Add(dc6);
                dtSchneiderData.Columns.Add(dc7);
                dtSchneiderData.Columns.Add(dc8);
                dtSchneiderData.Columns.Add(dc9);
                dtSchneiderData.Columns.Add(dc10);
                dtSchneiderData.Columns.Add(dc11);
                dtSchneiderData.Columns.Add(dc12);
                

            }



            dr[0] = First_Name;
            dr[1] = Last_Name;
            dr[2] = MobileNo;
            dr[3] = Email_ID;
            dr[4] = Additional_Email;
            dr[5] = Company_Name;
            dr[6] = Designation;
            dr[7] = Partner_ID;
            dr[8] = Zone;
            dr[9] = Address;
            dr[10] = City;
            dr[11] = State;
            dr[12] = "Not Processed Yet";
            dtSchneiderData.Rows.Add(dr);

        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            if (txtFile.Text.Trim().Length == 0)
                return;
            Upload();


            dgView.Columns[0].DataPropertyName = "First_Name";
            dgView.Columns[1].DataPropertyName = "Last_Name";
            dgView.Columns[2].DataPropertyName = "MobileNo";
            dgView.Columns[3].DataPropertyName = "Email_ID";
            dgView.Columns[4].DataPropertyName = "Additional_Email";
            dgView.Columns[5].DataPropertyName = "Company_Name";
            dgView.Columns[6].DataPropertyName = "Designation";
            dgView.Columns[7].DataPropertyName = "Partner_ID";
            dgView.Columns[8].DataPropertyName = "Zone";
            dgView.Columns[9].DataPropertyName = "Address";
            dgView.Columns[10].DataPropertyName = "City";
            dgView.Columns[11].DataPropertyName = "State";
            dgView.Columns[12].DataPropertyName = "UploadStatus";

            dgView.DataSource = dtSchneiderData;

            btnUpload.Enabled = false;
            btnProcess.Enabled = true;
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {


            string First_Name = "";
            string Last_Name = "";
            string MobileNo = "";
            string Email_ID = "";
            string Additional_Email = "";
            string Company_Name = "";
            string Designation = "";
            string Partner_ID = "";
            string Zone = "";
            string Address = "";
            string City = "";
            string State = "";

            string UploadStatus = "";


            string jsondata = "";
            servercomm sc = new servercomm();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            progressBar1.Visible = true;

            progressBar1.Minimum = 0;
            progressBar1.Value = 0;
            progressBar1.Maximum = dtSchneiderData.Rows.Count;

            for (int i = 0; i < dtSchneiderData.Rows.Count; i++)
            {
                try
                {

                    First_Name = dtSchneiderData.Rows[i][0].ToString();
                    Last_Name = dtSchneiderData.Rows[i][1].ToString();
                    MobileNo = dtSchneiderData.Rows[i][2].ToString();
                    Email_ID = dtSchneiderData.Rows[i][3].ToString();
                    Additional_Email = dtSchneiderData.Rows[i][4].ToString();
                    Company_Name = dtSchneiderData.Rows[i][5].ToString();
                    Designation = dtSchneiderData.Rows[i][6].ToString();
                    Partner_ID = dtSchneiderData.Rows[i][7].ToString();
                    Zone = dtSchneiderData.Rows[i][8].ToString();
                    Address = dtSchneiderData.Rows[i][9].ToString();
                    City = dtSchneiderData.Rows[i][10].ToString();
                    State = dtSchneiderData.Rows[i][11].ToString();

                    jsondata = PrepareData(First_Name, Last_Name, MobileNo, Email_ID, Additional_Email, Company_Name, Designation, Partner_ID, Zone, Address, City,State);

                    UploadStatus = sc.postRequest(UserImportUrl, jsondata);
                    UploadStatus = ser.Deserialize<String>(UploadStatus);

                    if (UploadStatus.Trim().ToUpper().Contains("SUCCESSFULLY"))
                    {
                        dtSchneiderData.Rows[i][12] = "Processed Successfully";
                    }
                    else if (UploadStatus.Trim().ToUpper().Contains("DUPLICATE"))
                    {
                        dtSchneiderData.Rows[i][12] = "Already Processed. Skipped";
                    }
                    else
                        dtSchneiderData.Rows[i][12] = UploadStatus;


                }
                catch (Exception ex)
                {
                    dtSchneiderData.Rows[i][12] = UploadStatus;
                }
                progressBar1.Value += 1;
                Application.DoEvents();
            }

            progressBar1.Visible = false;
            dtSchneiderData.AcceptChanges();
            dgView.DataSource = dtSchneiderData;
        }

        private string PrepareData(string First_Name, string Last_Name, string MobileNo, string Email_ID, string Additional_Email, string Company_Name, string Designation, string Partner_ID, string Zone, string Address, string City, string State)
        {
            JavaScriptSerializer ser = new JavaScriptSerializer();
            Dictionary<string, string> thisDict = new Dictionary<string, string>();

            string company_name = "";

            company_name = convertToString(((BatchProcess.frmImpDataForGrassreseller.ComboboxItem)(ddlComp.SelectedItem)).Text);

            if (company_name == "Select Company")
            {
                MessageBox.Show("Please Select A Company");
                
            }
            //SelesCycleId = convertToString(txtSalesCycleId.Text);

            thisDict.Add("first_name", First_Name);
            thisDict.Add("last_name", Last_Name);
            thisDict.Add("mobile_no", MobileNo);
            thisDict.Add("email_id", Email_ID);
            thisDict.Add("email_address", Additional_Email);
            thisDict.Add("company_name", Company_Name);
            thisDict.Add("designation", Designation);
            thisDict.Add("partner_id", Partner_ID);
            thisDict.Add("region", Zone);
            thisDict.Add("address", Address);
            thisDict.Add("city", City);
            thisDict.Add("district", State);
            thisDict.Add("company", company_name);
          
            string JSON = ser.Serialize(thisDict);

            return JSON;

        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            btnUpload.Enabled = true;
            btnProcess.Enabled = false;
            dtSchneiderData.Rows.Clear();
            dgView.DataSource = dtSchneiderData;
        }

        private void frmMigrate_Shown(object sender, EventArgs e)
        {
            btnProcess.Enabled = false;

        }




    }
}
