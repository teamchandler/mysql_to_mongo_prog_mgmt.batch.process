﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BatchProcess
{
    public partial class frmBatchConsole : Form
    {
        public frmBatchConsole()
        {
            InitializeComponent();
        }

        private void frmBatchConsole_Shown(object sender, EventArgs e)
        {
            login lgn = new login();
            /*
            lgn.TopLevel = false;
            lgn.AutoScroll = false;
            lgn.StartPosition = FormStartPosition.CenterParent;
            panel1.Controls.Add(lgn);
            lgn.StartPosition = FormStartPosition.CenterParent;
             */
            lgn.ShowDialog();
        }


        private void uploadPointsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCustomerPointUpload migrate = new frmCustomerPointUpload();

            migrate.TopLevel = false;
            migrate.AutoScroll = false;
            migrate.StartPosition = FormStartPosition.CenterParent;
            panel1.Controls.Add(migrate);
            migrate.StartPosition = FormStartPosition.CenterParent;
            migrate.WindowState = FormWindowState.Maximized;
            migrate.Show();
        }

        private void importCustomersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMigrate migrate = new frmMigrate();

            migrate.TopLevel = false;
            migrate.AutoScroll = false;
            migrate.StartPosition = FormStartPosition.CenterParent;
            panel1.Controls.Add(migrate);
            migrate.StartPosition = FormStartPosition.CenterParent;
            migrate.WindowState = FormWindowState.Maximized;
            migrate.Show();
        }

        private void importSalesTargetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmImpSalesTarget salestarget = new frmImpSalesTarget();

            salestarget.TopLevel = false;
            salestarget.AutoScroll = false;
            salestarget.StartPosition = FormStartPosition.CenterParent;
            panel1.Controls.Add(salestarget);
            salestarget.StartPosition = FormStartPosition.CenterParent;
            salestarget.WindowState = FormWindowState.Maximized;
            salestarget.Show();
        }

        private void importSalesTargetMonthToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmImpSalesTargetMonthly salestargetmonth = new frmImpSalesTargetMonthly();

            salestargetmonth.TopLevel = false;
            salestargetmonth.AutoScroll = false;
            salestargetmonth.StartPosition = FormStartPosition.CenterParent;
            panel1.Controls.Add(salestargetmonth);
            salestargetmonth.StartPosition = FormStartPosition.CenterParent;
            salestargetmonth.WindowState = FormWindowState.Maximized;
            salestargetmonth.Show();
        }

        private void importLavaDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmImpDataForLava ImpDataForLava = new frmImpDataForLava();

            ImpDataForLava.TopLevel = false;
            ImpDataForLava.AutoScroll = false;
            ImpDataForLava.StartPosition = FormStartPosition.CenterParent;
            panel1.Controls.Add(ImpDataForLava);
            ImpDataForLava.StartPosition = FormStartPosition.CenterParent;
            ImpDataForLava.WindowState = FormWindowState.Maximized;
            ImpDataForLava.Show();
        }

        private void importXoloDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmImpDataForXolo ImpDataForLava = new frmImpDataForXolo();

            ImpDataForLava.TopLevel = false;
            ImpDataForLava.AutoScroll = false;
            ImpDataForLava.StartPosition = FormStartPosition.CenterParent;
            panel1.Controls.Add(ImpDataForLava);
            ImpDataForLava.StartPosition = FormStartPosition.CenterParent;
            ImpDataForLava.WindowState = FormWindowState.Maximized;
            ImpDataForLava.Show();
        }


        private void importSchneiderDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmImpDataForSchneider ImpDataForLava = new frmImpDataForSchneider();

            ImpDataForLava.TopLevel = false;
            ImpDataForLava.AutoScroll = false;
            ImpDataForLava.StartPosition = FormStartPosition.CenterParent;
            panel1.Controls.Add(ImpDataForLava);
            ImpDataForLava.StartPosition = FormStartPosition.CenterParent;
            ImpDataForLava.WindowState = FormWindowState.Maximized;
            ImpDataForLava.Show();
        }


        private void importGrassresellerDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmImpDataForGrassreseller ImpDataForGrassreseller = new frmImpDataForGrassreseller();

            ImpDataForGrassreseller.TopLevel = false;
            ImpDataForGrassreseller.AutoScroll = false;
            ImpDataForGrassreseller.StartPosition = FormStartPosition.CenterParent;
            panel1.Controls.Add(ImpDataForGrassreseller);
            ImpDataForGrassreseller.StartPosition = FormStartPosition.CenterParent;
            ImpDataForGrassreseller.WindowState = FormWindowState.Maximized;
            ImpDataForGrassreseller.Show();
        }



        private void importSalesTargetMonthlyAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmImpSalesTargetMonthWise salestargetmonthwise = new frmImpSalesTargetMonthWise();

            salestargetmonthwise.TopLevel = false;
            salestargetmonthwise.AutoScroll = false;
            salestargetmonthwise.StartPosition = FormStartPosition.CenterParent;
            panel1.Controls.Add(salestargetmonthwise);
            salestargetmonthwise.StartPosition = FormStartPosition.CenterParent;
            salestargetmonthwise.WindowState = FormWindowState.Maximized;
            salestargetmonthwise.Show();
        }


        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


    }
}
