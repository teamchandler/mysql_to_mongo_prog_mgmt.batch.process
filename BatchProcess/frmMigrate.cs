﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace BatchProcess
{
    public partial class frmMigrate : Form
    {
        DataTable dtCustomer;
        string serverURL = "http://localhost:8185/user/postMigrateStore";
        public frmMigrate()
        {
            InitializeComponent();
            //serverURL = System.Configuration.ConfigurationSettings.AppSettings["migrate"].ToString();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtFile.Text = openFileDialog1.FileName;
            }
        }


        private void Upload()
        {
            System.IO.StreamReader sr = new System.IO.StreamReader(txtFile.Text);
            string strLine = "";
            int i = 0;
            string store = "";
            string FirstName = "";
            string LastName = "";
            string EmailId = "";
            string PointsLoaded = "";
            string PointsRedeemed = "";
            string ShippingAddress = "";

            dtCustomer = new DataTable();
            dtCustomer.Rows.Clear();

            while (!sr.EndOfStream)
            {
                i += 1;
                strLine = sr.ReadLine();

                if (i > 1)
                {
                    string[] strSplit = strLine.Split(';');

                    if (strSplit.Length >= 7)
                    {
                        store = strSplit[0];
                        FirstName = strSplit[1];
                        LastName = strSplit[2];
                        EmailId = strSplit[3];
                        PointsLoaded = strSplit[4];
                        PointsRedeemed = strSplit[5];
                        ShippingAddress = strSplit[6];

                        addtoDataTable(store, FirstName, LastName, EmailId, PointsLoaded, PointsRedeemed, ShippingAddress);
                    }
                }
            }

        }

        private void addtoDataTable(string store, string FirstName, string LastName, string EmailId, string PointsLoaded, string PointsRedeemed, string ShippingAddress)
        {
            DataRow dr = dtCustomer.NewRow();

            if (dtCustomer.Columns.Count == 0)
            {
                DataColumn dc0 = new DataColumn("Store");
                DataColumn dc1 = new DataColumn("First_Name");
                DataColumn dc2 = new DataColumn("Last_Name");
                DataColumn dc3 = new DataColumn("User_ID");
                DataColumn dc4 = new DataColumn("Points_Loaded");
                DataColumn dc5 = new DataColumn("Points_Redeemed");
                DataColumn dc6 = new DataColumn("Shipping_Address");
                DataColumn dc7 = new DataColumn("Status");

                dtCustomer.Columns.Add(dc0);
                dtCustomer.Columns.Add(dc1);
                dtCustomer.Columns.Add(dc2);
                dtCustomer.Columns.Add(dc3);
                dtCustomer.Columns.Add(dc4);
                dtCustomer.Columns.Add(dc5);
                dtCustomer.Columns.Add(dc6);
                dtCustomer.Columns.Add(dc7);
            }

            dr[0] = store;
            dr[1] = FirstName;
            dr[2] = LastName;
            dr[3] = EmailId;
            dr[4] = PointsLoaded;
            dr[5] = PointsRedeemed;
            dr[6] = ShippingAddress;
            dr[7] = "Not Processed Yet";
            dtCustomer.Rows.Add(dr);

        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            if (txtFile.Text.Trim().Length == 0)
                return;
            Upload();

            dgView.Columns[0].DataPropertyName = "Store";
            dgView.Columns[1].DataPropertyName = "First_Name";
            dgView.Columns[2].DataPropertyName = "Last_Name";
            dgView.Columns[3].DataPropertyName = "User_ID";
            dgView.Columns[4].DataPropertyName = "Points_Loaded";
            dgView.Columns[5].DataPropertyName = "Points_Redeemed";
            dgView.Columns[6].DataPropertyName = "Shipping_Address";
            dgView.Columns[7].DataPropertyName = "Status";
            dgView.DataSource = dtCustomer;

            btnUpload.Enabled = false;
            btnProcess.Enabled = true;
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            string store = "";
            string FirstName = "";
            string LastName = "";
            string EmailId = "";
            string PointsLoaded = "";
            string PointsRedeemed = "";
            string ShippingAddress = "";
            string jsondata = "";
            string status = "";
            servercomm sc = new servercomm();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            progressBar1.Visible = true;

            progressBar1.Minimum = 0;
            progressBar1.Value = 0;
            progressBar1.Maximum = dtCustomer.Rows.Count;

            for (int i = 0; i < dtCustomer.Rows.Count; i++)
            {
                try
                {
                    store = dtCustomer.Rows[i][0].ToString();
                    FirstName = dtCustomer.Rows[i][1].ToString();
                    LastName = dtCustomer.Rows[i][2].ToString();
                    EmailId = dtCustomer.Rows[i][3].ToString();
                    PointsLoaded = dtCustomer.Rows[i][4].ToString();
                    PointsRedeemed = dtCustomer.Rows[i][5].ToString();
                    ShippingAddress = dtCustomer.Rows[i][6].ToString();

                    jsondata = PrepareData(store, FirstName, LastName, EmailId, PointsLoaded, PointsRedeemed, ShippingAddress);
                    
                    status = sc.postRequest(serverURL, jsondata);
                    status = ser.Deserialize<String>(status);

                    if (status.Trim().ToUpper().Contains("SUCCESSFULLY"))
                    {
                        dtCustomer.Rows[i][7] = "Processed Successfully";
                    }
                    else if (status.Trim().ToUpper().Contains("DUPLICATE"))
                    {
                        dtCustomer.Rows[i][7] = "Already Processed. Skipped";
                    }
                    else
                        dtCustomer.Rows[i][7] = status;


                }
                catch(Exception ex)
                {
                    dtCustomer.Rows[i][7] = status; 
                }
                progressBar1.Value += 1;
                Application.DoEvents();
            }

            progressBar1.Visible = false;
            dtCustomer.AcceptChanges();
            dgView.DataSource = dtCustomer;
        }

        private string PrepareData(string store, string FirstName, string LastName, string EmailId, string PointsLoaded, string PointsRedeemed, string ShippingAddress)
        {
            JavaScriptSerializer ser = new JavaScriptSerializer();
            Dictionary<string, string> thisDict = new Dictionary<string, string>();
            thisDict.Add("store", store);
            thisDict.Add("firstname", FirstName);
            thisDict.Add("lastname", LastName);
            thisDict.Add("emailid", EmailId);
            thisDict.Add("pointsloaded", PointsLoaded);
            thisDict.Add("pointsredeemed", PointsRedeemed);
            thisDict.Add("shippingaddress", ShippingAddress);
            string JSON = ser.Serialize(thisDict);
            
            return JSON;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            btnUpload.Enabled = true;
            btnProcess.Enabled = false;
            dtCustomer.Rows.Clear();
            dgView.DataSource = dtCustomer;
        }

        private void frmMigrate_Shown(object sender, EventArgs e)
        {
            btnProcess.Enabled = false;
            
        }

       
    }
}
