﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace BatchProcess
{
    public partial class frmImpSalesTargetMonthWise : Form
    {
        DataTable dtTargetSalesMonthWise;
        string serverURL = "http://localhost:8185/user/postSalesTargetMonthWise";
        public frmImpSalesTargetMonthWise()
        {
            InitializeComponent();
            //serverURL = System.Configuration.ConfigurationSettings.AppSettings["SalesTargetMonthWise"].ToString();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtFile.Text = openFileDialog1.FileName;
            }
        }


        private void Upload()
        {
            System.IO.StreamReader sr = new System.IO.StreamReader(txtFile.Text);

            string strLine = "";
            int i = 0;

            string Mobile = "";
            string Email = "";
            string Target = "";
            string Achievement = "";
            string Points = "";
            string StartDate = "";
            string EndDate = "";

            dtTargetSalesMonthWise = new DataTable();
            dtTargetSalesMonthWise.Rows.Clear();

            while (!sr.EndOfStream)
            {
                i += 1;
                strLine = sr.ReadLine();

                if (i > 1)
                {
                    string[] strSplit = strLine.Split(';');

                    if (strSplit.Length >= 7)
                    {
                        Mobile = strSplit[0];
                        Email = strSplit[1];
                        Target = strSplit[2];
                        Achievement = strSplit[3];
                        Points = strSplit[4];
                        StartDate = strSplit[5];
                        EndDate = strSplit[6];

                        addtoDataTable(Mobile, Email, Target, Achievement, Points, StartDate, EndDate);
                    }
                }
            }

        }

        private void addtoDataTable(string Mobile, string Email, string Target, string Achievement, string Points, string StartDate, string EndDate)
        {

            DataRow dr = dtTargetSalesMonthWise.NewRow();

            if (dtTargetSalesMonthWise.Columns.Count == 0)
            {

                DataColumn dc0 = new DataColumn("Mobile");
                DataColumn dc1 = new DataColumn("Email");
                DataColumn dc2 = new DataColumn("Target");
                DataColumn dc3 = new DataColumn("Achievement");
                DataColumn dc4 = new DataColumn("Points");
                DataColumn dc5 = new DataColumn("Start_Date");
                DataColumn dc6 = new DataColumn("End_Date");
                DataColumn dc7 = new DataColumn("Status");


                dtTargetSalesMonthWise.Columns.Add(dc0);
                dtTargetSalesMonthWise.Columns.Add(dc1);
                dtTargetSalesMonthWise.Columns.Add(dc2);
                dtTargetSalesMonthWise.Columns.Add(dc3);
                dtTargetSalesMonthWise.Columns.Add(dc4);
                dtTargetSalesMonthWise.Columns.Add(dc5);
                dtTargetSalesMonthWise.Columns.Add(dc6);
                dtTargetSalesMonthWise.Columns.Add(dc7);

            }

            dr[0] = Mobile;
            dr[1] = Email;
            dr[2] = Target;
            dr[3] = Achievement;
            dr[4] = Points;
            dr[5] = StartDate;
            dr[6] = EndDate;
            dr[7] = "Not Processed Yet";
            dtTargetSalesMonthWise.Rows.Add(dr);

        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            if (txtFile.Text.Trim().Length == 0)
                return;
            Upload();

            dgView.Columns[0].DataPropertyName = "Mobile";
            dgView.Columns[1].DataPropertyName = "Email";
            dgView.Columns[2].DataPropertyName = "Target";
            dgView.Columns[3].DataPropertyName = "Achievement";
            dgView.Columns[4].DataPropertyName = "Points";
            dgView.Columns[5].DataPropertyName = "Start_Date";
            dgView.Columns[6].DataPropertyName = "End_Date";
            dgView.Columns[7].DataPropertyName = "Status";

            dgView.DataSource = dtTargetSalesMonthWise;

            btnUpload.Enabled = false;
            btnProcess.Enabled = true;
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {

            string Mobile = "";
            string Email = "";
            string Target = "";
            string Achievement = "";
            string Points = "";
            string StartDate = "";
            string EndDate = "";
            string status = "";
            string jsondata = "";
            servercomm sc = new servercomm();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            progressBar1.Visible = true;

            progressBar1.Minimum = 0;
            progressBar1.Value = 0;
            progressBar1.Maximum = dtTargetSalesMonthWise.Rows.Count;

            for (int i = 0; i < dtTargetSalesMonthWise.Rows.Count; i++)
            {
                try
                {

                    Mobile = dtTargetSalesMonthWise.Rows[i][0].ToString();
                    Email = dtTargetSalesMonthWise.Rows[i][1].ToString();
                    Target = dtTargetSalesMonthWise.Rows[i][2].ToString();
                    Achievement = dtTargetSalesMonthWise.Rows[i][3].ToString();
                    Points = dtTargetSalesMonthWise.Rows[i][4].ToString();
                    StartDate = dtTargetSalesMonthWise.Rows[i][5].ToString();
                    EndDate = dtTargetSalesMonthWise.Rows[i][6].ToString();

                    jsondata = PrepareData(Mobile, Email, Target, Achievement, Points, StartDate, EndDate);

                    status = sc.postRequest(serverURL, jsondata);
                    status = ser.Deserialize<String>(status);

                    if (status.Trim().ToUpper().Contains("SUCCESSFULLY"))
                    {
                        dtTargetSalesMonthWise.Rows[i][7] = "Processed Successfully";
                    }
                    else if (status.Trim().ToUpper().Contains("DUPLICATE"))
                    {
                        dtTargetSalesMonthWise.Rows[i][7] = "Already Processed. Skipped";
                    }
                    else
                        dtTargetSalesMonthWise.Rows[i][7] = status;


                }
                catch (Exception ex)
                {
                    dtTargetSalesMonthWise.Rows[i][7] = status;
                }
                progressBar1.Value += 1;
                Application.DoEvents();
            }

            progressBar1.Visible = false;
            dtTargetSalesMonthWise.AcceptChanges();
            dgView.DataSource = dtTargetSalesMonthWise;
        }

        private string PrepareData(string Mobile, string Email, string Target, string Achievement, string Points, string StartDate, string EndDate)
        {
            JavaScriptSerializer ser = new JavaScriptSerializer();
            Dictionary<string, string> thisDict = new Dictionary<string, string>();

            thisDict.Add("mobile_no", Mobile);    
            thisDict.Add("email_id", Email);
            thisDict.Add("Target", Target);
            thisDict.Add("Achievement", Achievement);
            thisDict.Add("Points", Points);
            thisDict.Add("Start_Date", StartDate);
            thisDict.Add("End_Date", EndDate);

            string JSON = ser.Serialize(thisDict);

            return JSON;

        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            btnUpload.Enabled = true;
            btnProcess.Enabled = false;
            dtTargetSalesMonthWise.Rows.Clear();
            dgView.DataSource = dtTargetSalesMonthWise;
        }

        private void frmMigrate_Shown(object sender, EventArgs e)
        {
            btnProcess.Enabled = false;

        }


    }
}
