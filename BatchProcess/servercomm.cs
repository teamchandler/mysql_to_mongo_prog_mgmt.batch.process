﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BatchProcess
{
    public class servercomm
    {
        //send a POST request
        public string postRequest(string url, string data)
        {
            //send request to server
            var result = "";
            var request = (HttpWebRequest)WebRequest.Create(url);
            //request.CookieContainer = cookies;
            request.Method = "POST";
            Console.WriteLine("Sending POST request...");
            request.ContentType = "application/json"; //POSTing a json
            using (var requestStream = request.GetRequestStream())
            {
                using (var writer = new StreamWriter(requestStream))
                {
                    Console.WriteLine("Logging in via POST request...");
                    writer.Write(data); //write the data to the server
                    Console.WriteLine("Submitted data to server");
                }
            }

            //get response from server
            Console.WriteLine("Reading response from server...");
            using (var responseFeed = request.GetResponse())
            {
                using (var responseStream = responseFeed.GetResponseStream())
                {
                    using (var reader = new StreamReader(responseStream))
                    {
                        result = reader.ReadToEnd();
                        Console.WriteLine("Read response from server");
                        //Console.WriteLine(result); //print the result from the server
                    }
                }
            }
            
            return result;
        }


        public string getRequest(string url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            //request.CookieContainer = cookies;
            request.Method = "GET";
            Console.WriteLine("Sending GET request...");
            using (var responseStream = request.GetResponse().GetResponseStream())
            {
                using (var reader = new StreamReader(responseStream))
                {
                    var result = reader.ReadToEnd();
                    Console.WriteLine(result);
                    Console.WriteLine("GET request successfull");
                    return result;
                }
            }
        }

    }
}
