﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace BatchProcess
{
    public partial class login : Form
    {
        string serverURL = "";

        public login()
        {
            InitializeComponent();

            serverURL = System.Configuration.ConfigurationSettings.AppSettings["login"].ToString();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            lblMsg.Text = "Please Wait";
            btnSubmit.Enabled = false;
            if (IsValid())
                Close();

            btnSubmit.Enabled = true;
            lblMsg.Text = "";
        }

        public Boolean IsValid()
        {
            servercomm sc = new servercomm();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            string jsondata = PrepareData(0,textBox1.Text,82,textBox2.Text,"");
            var result = sc.postRequest(serverURL, jsondata);

            login_msg lm = ser.Deserialize<login_msg>(result);

            if (lm.value.Trim().ToUpper() == "LOG IN SUCCESSFULLY")
            {
                return true;
            }
            else
            {
                MessageBox.Show("Message from Server : " + lm.value);
                return false;
            }
            

        }

        public string PrepareData(Int64 Id,string user_name,Int64 company_id,string password, string user_type)
        {
            JavaScriptSerializer ser = new JavaScriptSerializer();
            Dictionary<string, string> thisDict = new Dictionary<string, string>();
            thisDict.Add("Id", Id.ToString());
            thisDict.Add("user_name", user_name);
            thisDict.Add("company_id", company_id.ToString());
            thisDict.Add("password", password);
            thisDict.Add("user_type", user_type);
            string JSON = ser.Serialize(thisDict);

            return JSON;

        }
    }
}
