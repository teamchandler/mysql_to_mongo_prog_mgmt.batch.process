﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace BatchProcess
{
    public partial class frmImpDataForXolo : Form
    {
        DataTable dtXoloData;
        //string serverURL = "http://localhost:8185/user/postLavaData";
        //string GetCompURL = "http://localhost:8185/user/admin/postPopulateCompany";

        string serverURL = "";
        string GetCompURL = "";
        string LavaUrl = "";

        public frmImpDataForXolo()
        {
            InitializeComponent();
            serverURL = System.Configuration.ConfigurationSettings.AppSettings["ServerURL"].ToString();
            LavaUrl = serverURL + "/user/postXoloData";
            GetCompURL = serverURL + "/user/admin/postPopulateCompany";

            PopulateCompany();

        }

        private void btnSelect_Click(object sender, EventArgs e)
        {

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtFile.Text = openFileDialog1.FileName;
            }
        }


        private void PopulateCompany()
        {

            string CompData = "";
            List<COMPANY> CompList = new List<COMPANY>();
            servercomm scc = new servercomm();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            CompData = scc.postRequest(GetCompURL, "");
            CompList = ser.Deserialize<List<COMPANY>>(CompData);

            if (CompList != null && CompList.Count() > 0)
            {
                List<ComboboxItem> liCombo = new List<ComboboxItem>();
                for (int i = 1; i < CompList.Count(); i++)
                {
                    ComboboxItem item = new ComboboxItem();
                    item.Text = CompList[i].name;
                    item.Value = CompList[i].id;
                    liCombo.Add(item);
                }
                this.ddlComp.DataSource = liCombo;
            }


        }

        private class ComboboxItem
        {
            public string Text { get; set; }
            public object Value { get; set; }

            public override string ToString()
            {
                return Text;
            }
        }
        private string convertToString(object o)
        {
            try
            {
                if ((o != null) && (o.ToString() != ""))
                {
                    return o.ToString().Trim();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {

                return "";
            }
        }

        private class COMPANY
        {
            public long id { get; set; }
            public string name { get; set; }
            public string displayname { get; set; }
            public string contactname { get; set; }
            public string phonenumber { get; set; }
            public string address { get; set; }
            public string emailid { get; set; }
            public string logo { get; set; }
            public decimal point_range_max { get; set; }
            public decimal point_range_min { get; set; }
            public string root_url_1 { get; set; }
            public string root_url_2 { get; set; }
            public decimal shipping_charge { get; set; }
            public decimal min_order_for_free_shipping { get; set; }


            public int sales_tracking { get; set; }
            public string track_cycle { get; set; }
            public int validity { get; set; }

        }
        private void Upload()
        {
            System.IO.StreamReader sr = new System.IO.StreamReader(txtFile.Text);

            string strLine = "";
            int i = 0;

            string Emp_Id = "";
            string BU = "";
            string Branch = "";
            string Outlet_Code = "";
            string Outlet_Name = "";
            string FOS_DSE_CODE = "";
            string First_Name = "";
            string Last_Name = "";
            string FOS_DSE_Mail_Id = "";
            string DSE_Contact_Number = "";
            string TM_Name = "";
            string TM_Code = "";
            string Address = "";
            string City = "";
            string District = "";
            string DOB = "";
            string DOJ = "";
            string Qualifiations = "";
            string CompanyId = "";
            string SelesCycleId = "";



            dtXoloData = new DataTable();
            dtXoloData.Rows.Clear();

            while (!sr.EndOfStream)
            {
                i += 1;
                strLine = sr.ReadLine();

                if (i > 1)
                {
                    string[] strSplit = strLine.Split(';');

                    if (strSplit.Length >= 13)
                    {

                        Emp_Id = convertToString(strSplit[0]);
                        BU = convertToString(strSplit[1]);
                        Branch = convertToString(strSplit[2]);
                        Outlet_Code = convertToString(strSplit[3]);
                        Outlet_Name = convertToString(strSplit[4]);
                        FOS_DSE_CODE = convertToString(strSplit[5]);
                        First_Name = convertToString(strSplit[6]);
                        Last_Name = convertToString(strSplit[7]);
                        FOS_DSE_Mail_Id = convertToString(strSplit[8]);
                        DSE_Contact_Number = convertToString(strSplit[9]);
                        TM_Name = convertToString(strSplit[10]);
                        TM_Code = convertToString(strSplit[11]);
                        Address = convertToString(strSplit[12]);
                        City = convertToString(strSplit[13]);
                        District = convertToString(strSplit[14]);
                        DOB = convertToString(strSplit[15]);
                        DOJ = convertToString(strSplit[16]);
                        Qualifiations = convertToString(strSplit[17]);
                        addtoDataTable(Emp_Id, BU, Branch, Outlet_Code, Outlet_Name, FOS_DSE_CODE, First_Name, Last_Name, FOS_DSE_Mail_Id, DSE_Contact_Number, TM_Name, TM_Code, Address, City, District, DOB, DOJ, Qualifiations);

                    }
                }
            }

        }

        private void addtoDataTable(string Emp_Id, string BU, string Branch, string Outlet_Code, string Outlet_Name, string FOS_DSE_CODE, string First_Name, string Last_Name, string FOS_DSE_Mail_Id, string DSE_Contact_Number, string TM_Name, string TM_Code, string Address, string City, string District, string DOB, string DOJ, string Qualifiations)
        {


            DataRow dr = dtXoloData.NewRow();

            if (dtXoloData.Columns.Count == 0)
            {
                DataColumn dc0 = new DataColumn("Emp_Id");
                DataColumn dc1 = new DataColumn("BU");
                DataColumn dc2 = new DataColumn("Branch");
                DataColumn dc3 = new DataColumn("Outlet_Code");
                DataColumn dc4 = new DataColumn("Outlet_Name");
                DataColumn dc5 = new DataColumn("FOS_DSE_CODE");
                DataColumn dc6 = new DataColumn("First_Name");
                DataColumn dc7 = new DataColumn("Last_Name");
                DataColumn dc8 = new DataColumn("FOS_DSE_Mail_Id");
                DataColumn dc9 = new DataColumn("DSE_Contact_Number");
                DataColumn dc10 = new DataColumn("TM_Name");
                DataColumn dc11 = new DataColumn("TM_Code");
                DataColumn dc12 = new DataColumn("Address");
                DataColumn dc13 = new DataColumn("City");
                DataColumn dc14 = new DataColumn("District");
                DataColumn dc15 = new DataColumn("DOB");
                DataColumn dc16 = new DataColumn("DOJ");
                DataColumn dc17 = new DataColumn("Qualifiations");
                DataColumn dc18 = new DataColumn("UploadStatus");


                dtXoloData.Columns.Add(dc0);
                dtXoloData.Columns.Add(dc1);
                dtXoloData.Columns.Add(dc2);
                dtXoloData.Columns.Add(dc3);
                dtXoloData.Columns.Add(dc4);
                dtXoloData.Columns.Add(dc5);
                dtXoloData.Columns.Add(dc6);
                dtXoloData.Columns.Add(dc7);
                dtXoloData.Columns.Add(dc8);
                dtXoloData.Columns.Add(dc9);
                dtXoloData.Columns.Add(dc10);
                dtXoloData.Columns.Add(dc11);
                dtXoloData.Columns.Add(dc12);
                dtXoloData.Columns.Add(dc13);
                dtXoloData.Columns.Add(dc14);
                dtXoloData.Columns.Add(dc15);
                dtXoloData.Columns.Add(dc16);
                dtXoloData.Columns.Add(dc17);
                dtXoloData.Columns.Add(dc18);

            }



            dr[0] = Emp_Id;
            dr[1] = BU;
            dr[2] = Branch;
            dr[3] = Outlet_Code;
            dr[4] = Outlet_Name;
            dr[5] = FOS_DSE_CODE;
            dr[6] = First_Name;
            dr[7] = Last_Name;
            dr[8] = FOS_DSE_Mail_Id;
            dr[9] = DSE_Contact_Number;
            dr[10] = TM_Name;
            dr[11] = TM_Code;
            dr[12] = Address;
            dr[13] = City;
            dr[14] = District;
            dr[15] = DOB;
            dr[16] = DOJ;
            dr[17] = Qualifiations;

            dr[18] = "Not Processed Yet";
            dtXoloData.Rows.Add(dr);

        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            if (txtFile.Text.Trim().Length == 0)
                return;
            Upload();


            dgView.Columns[0].DataPropertyName = "Emp_Id";
            dgView.Columns[1].DataPropertyName = "BU";
            dgView.Columns[2].DataPropertyName = "Branch";
            dgView.Columns[3].DataPropertyName = "Outlet_Code";
            dgView.Columns[4].DataPropertyName = "Outlet_Name";
            dgView.Columns[5].DataPropertyName = "FOS_DSE_CODE";
            dgView.Columns[6].DataPropertyName = "First_Name";
            dgView.Columns[7].DataPropertyName = "Last_Name";
            dgView.Columns[8].DataPropertyName = "FOS_DSE_Mail_Id";
            dgView.Columns[9].DataPropertyName = "DSE_Contact_Number";
            dgView.Columns[10].DataPropertyName = "TM_Name";
            dgView.Columns[11].DataPropertyName = "TM_Code";
            dgView.Columns[12].DataPropertyName = "Address";
            dgView.Columns[13].DataPropertyName = "City";
            dgView.Columns[14].DataPropertyName = "District";
            dgView.Columns[15].DataPropertyName = "DOB";
            dgView.Columns[16].DataPropertyName = "DOJ";
            dgView.Columns[17].DataPropertyName = "Qualifiations";
            dgView.Columns[18].DataPropertyName = "UploadStatus";

            dgView.DataSource = dtXoloData;

            btnUpload.Enabled = false;
            btnProcess.Enabled = true;
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {


            string Emp_Id = "";
            string BU = "";
            string Branch = "";
            string Outlet_Code = "";
            string Outlet_Name = "";
            string FOS_DSE_CODE = "";
            string First_Name = "";
            string Last_Name = "";
            string FOS_DSE_Mail_Id = "";
            string DSE_Contact_Number = "";
            string TM_Name = "";
            string TM_Code = "";
            string Address = "";
            string City = "";
            string District = "";
            string DOB = "";
            string DOJ = "";
            string Qualifiations = "";

            string UploadStatus = "";


            string jsondata = "";
            servercomm sc = new servercomm();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            progressBar1.Visible = true;

            progressBar1.Minimum = 0;
            progressBar1.Value = 0;
            progressBar1.Maximum = dtXoloData.Rows.Count;

            for (int i = 0; i < dtXoloData.Rows.Count; i++)
            {
                try
                {

                    Emp_Id = dtXoloData.Rows[i][0].ToString();
                    BU = dtXoloData.Rows[i][1].ToString();
                    Branch = dtXoloData.Rows[i][2].ToString();
                    Outlet_Code = dtXoloData.Rows[i][3].ToString();
                    Outlet_Name = dtXoloData.Rows[i][4].ToString();
                    FOS_DSE_CODE = dtXoloData.Rows[i][5].ToString();
                    First_Name = dtXoloData.Rows[i][6].ToString();
                    Last_Name = dtXoloData.Rows[i][7].ToString();
                    FOS_DSE_Mail_Id = dtXoloData.Rows[i][8].ToString();
                    DSE_Contact_Number = dtXoloData.Rows[i][9].ToString();
                    TM_Name = dtXoloData.Rows[i][10].ToString();
                    TM_Code = dtXoloData.Rows[i][11].ToString();
                    Address = dtXoloData.Rows[i][12].ToString();
                    City = dtXoloData.Rows[i][13].ToString();
                    District = dtXoloData.Rows[i][14].ToString();
                    DOB = dtXoloData.Rows[i][15].ToString();
                    DOJ = dtXoloData.Rows[i][16].ToString();
                    Qualifiations = dtXoloData.Rows[i][17].ToString();


                    jsondata = PrepareData(Emp_Id, BU, Branch, Outlet_Code, Outlet_Name, FOS_DSE_CODE, First_Name, Last_Name, FOS_DSE_Mail_Id, DSE_Contact_Number, TM_Name, TM_Code, Address, City, District, DOB, DOJ, Qualifiations);

                    UploadStatus = sc.postRequest(LavaUrl, jsondata);
                    UploadStatus = ser.Deserialize<String>(UploadStatus);

                    if (UploadStatus.Trim().ToUpper().Contains("SUCCESSFULLY"))
                    {
                        dtXoloData.Rows[i][18] = "Processed Successfully";
                    }
                    else if (UploadStatus.Trim().ToUpper().Contains("DUPLICATE"))
                    {
                        dtXoloData.Rows[i][18] = "Already Processed. Skipped";
                    }
                    else
                        dtXoloData.Rows[i][18] = UploadStatus;


                }
                catch (Exception ex)
                {
                    dtXoloData.Rows[i][18] = UploadStatus;
                }
                progressBar1.Value += 1;
                Application.DoEvents();
            }

            progressBar1.Visible = false;
            dtXoloData.AcceptChanges();
            dgView.DataSource = dtXoloData;
        }

        private string PrepareData(string Emp_Id, string BU, string Branch, string Outlet_Code, string Outlet_Name, string FOS_DSE_CODE, string First_Name, string Last_Name, string FOS_DSE_Mail_Id, string DSE_Contact_Number, string TM_Name, string TM_Code, string Address, string City, string District, string DOB, string DOJ, string Qualifiations)
        {
            JavaScriptSerializer ser = new JavaScriptSerializer();
            Dictionary<string, string> thisDict = new Dictionary<string, string>();

            string CompanyId = "";
            string SelesCycleId = "";
            CompanyId = convertToString(((BatchProcess.frmImpDataForXolo.ComboboxItem)(ddlComp.SelectedItem)).Value);
            SelesCycleId = convertToString(txtSalesCycleId.Text);

            thisDict.Add("Emp_Id", Emp_Id);
            thisDict.Add("BU", BU);
            thisDict.Add("Branch", Branch);
            thisDict.Add("Outlet_Code", Outlet_Code);
            thisDict.Add("Outlet_Name", Outlet_Name);
            thisDict.Add("FOS_DSE_CODE", FOS_DSE_CODE);
            thisDict.Add("First_Name", First_Name);
            thisDict.Add("Last_Name", Last_Name);
            thisDict.Add("FOS_DSE_Mail_Id", FOS_DSE_Mail_Id);
            thisDict.Add("DSE_Contact_Number", DSE_Contact_Number);
            thisDict.Add("TM_Name", TM_Name);
            thisDict.Add("TM_Code", TM_Code);
            thisDict.Add("Address", Address);
            thisDict.Add("City", City);
            thisDict.Add("District", District);
            thisDict.Add("DOB", DOB);
            thisDict.Add("DOJ", DOJ);
            thisDict.Add("Qualifiations", Qualifiations);
            thisDict.Add("CompanyId", CompanyId);
            thisDict.Add("SelesCycleId", SelesCycleId);

            string JSON = ser.Serialize(thisDict);

            return JSON;

        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            btnUpload.Enabled = true;
            btnProcess.Enabled = false;
            dtXoloData.Rows.Clear();
            dgView.DataSource = dtXoloData;
        }

        private void frmMigrate_Shown(object sender, EventArgs e)
        {
            btnProcess.Enabled = false;

        }




    }
}
