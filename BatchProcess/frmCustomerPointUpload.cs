﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace BatchProcess
{
    public partial class frmCustomerPointUpload : Form
    {
        DataTable dtCustomer;

        string serverURL = "";
        string postapi = "";
        
        public frmCustomerPointUpload()
        {
            InitializeComponent();
            serverURL = System.Configuration.ConfigurationSettings.AppSettings["ServerURL"].ToString();
            postapi = serverURL + "/user/postUploadCustPoints";
           
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtFile.Text = openFileDialog1.FileName;
            }
        }


        private void Upload()
        {
            System.IO.StreamReader sr = new System.IO.StreamReader(txtFile.Text);
            string strLine = "";
            int i = 0;
        
            string UserId = "";
            string UploadPoints = "";
        
            dtCustomer = new DataTable();
            dtCustomer.Rows.Clear();

            while (!sr.EndOfStream)
            {
                i += 1;
                strLine = sr.ReadLine();

                if (i > 1)
                {
                    string[] strSplit = strLine.Split(';');

                    if (strSplit.Length >= 2)
                    {
                        UserId = strSplit[0];
                        UploadPoints = strSplit[1];

                        addtoDataTable(UserId, UploadPoints);
                    }
                }
            }

        }

        private void addtoDataTable(string UserId, string UploadPoints)
        {
            DataRow dr = dtCustomer.NewRow();

            if (dtCustomer.Columns.Count == 0)
            {
                DataColumn dc0 = new DataColumn("UserID");
                DataColumn dc1 = new DataColumn("UploadPoints");
                DataColumn dc2 = new DataColumn("UploadStatus");
                
                dtCustomer.Columns.Add(dc0);
                dtCustomer.Columns.Add(dc1);
                dtCustomer.Columns.Add(dc2);
                
            }

            dr[0] = UserId;
            dr[1] = UploadPoints;     
            dr[2] = "Not Processed Yet";
            dtCustomer.Rows.Add(dr);

        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            if (txtFile.Text.Trim().Length == 0)
                return;
            Upload();

            dgView.Columns[0].DataPropertyName = "UserID";
            dgView.Columns[1].DataPropertyName = "UploadPoints";
            dgView.Columns[2].DataPropertyName = "UploadStatus";
            dgView.DataSource = dtCustomer;

            btnUpload.Enabled = false;
            btnProcess.Enabled = true;
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            string UserId = "";
            string UploadPoints = "";          
            string jsondata = "";
            string status = "";
            servercomm sc = new servercomm();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            progressBar1.Visible = true;

            progressBar1.Minimum = 0;
            progressBar1.Value = 0;
            progressBar1.Maximum = dtCustomer.Rows.Count;

            for (int i = 0; i < dtCustomer.Rows.Count; i++)
            {
                try
                {
                    UserId = dtCustomer.Rows[i][0].ToString();
                    UploadPoints = dtCustomer.Rows[i][1].ToString();

                    jsondata = PrepareData(UserId, UploadPoints);

                    status = sc.postRequest(postapi, jsondata);
                    status = ser.Deserialize<String>(status);

                    if (status.Trim().ToUpper().Contains("SUCCESSFULLY"))
                    {
                        dtCustomer.Rows[i][2] = "Processed Successfully";
                    }
                    else if (status.Trim().ToUpper().Contains("DUPLICATE"))
                    {
                        dtCustomer.Rows[i][2] = "Already Processed. Skipped";
                    }
                    else
                        dtCustomer.Rows[i][2] = status;


                }
                catch(Exception ex)
                {
                    dtCustomer.Rows[i][2] = status; 
                }
                progressBar1.Value += 1;
                Application.DoEvents();
            }

            progressBar1.Visible = false;
            dtCustomer.AcceptChanges();
            dgView.DataSource = dtCustomer;
        }

        private string PrepareData(string UserId, string UploadPoints)
        {
            JavaScriptSerializer ser = new JavaScriptSerializer();
            Dictionary<string, string> thisDict = new Dictionary<string, string>();

            thisDict.Add("emailid", UserId);
            thisDict.Add("pointsloaded", UploadPoints);
           
            string JSON = ser.Serialize(thisDict);
            
            return JSON;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            btnUpload.Enabled = true;
            btnProcess.Enabled = false;
            dtCustomer.Rows.Clear();
            dgView.DataSource = dtCustomer;
        }

        private void frmMigrate_Shown(object sender, EventArgs e)
        {
            btnProcess.Enabled = false;
            
        }

       
    }
}
